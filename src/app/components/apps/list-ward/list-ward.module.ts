import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { ListWardRoutingModule } from './list-ward-routing.module';
import { ListWardComponent } from './list-ward.component';
import { AvatarModule } from 'primeng/avatar';
import { DropdownModule } from 'primeng/dropdown';

import { DataViewModule } from 'primeng/dataview';

import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { AutoCompleteModule } from 'primeng/autocomplete';

import { NgxSpinnerModule } from "ngx-spinner";
import {SharedModule} from '../../../shared/sharedModule'

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';


import { ChipsModule } from 'primeng/chips';







@NgModule({
  declarations: [
    ListWardComponent
  ],
  imports: [
    CommonModule,
    ListWardRoutingModule,FormsModule,ReactiveFormsModule ,AutoCompleteModule,SharedModule,
    AvatarModule,DropdownModule,DataViewModule,ButtonModule,DividerModule,NgxSpinnerModule,
   ChipsModule,
  ]
})
export class ListWardModule { }
