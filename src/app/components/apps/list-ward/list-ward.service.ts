import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListWardService {

  pathPrefixLookup: any = `api-lookup/lookup`;
  pathPrefixDoctor: any = `api-doctor/doctor`;
  pathPrefixNurse: any = `api-nurse/nurse`;
  pathPrefixAuth: any = `api-auth/auth`;

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  });

  constructor () {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async getWard() {
    const url = `/ward`;
    return this.axiosInstance.get(url);
  }

}
