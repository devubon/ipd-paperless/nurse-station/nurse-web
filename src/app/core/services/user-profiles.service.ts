import { Injectable } from '@angular/core'
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UserProfileService {
  public globalVar = 'Oll  value';
  public scrHeight = '100px';
  public scrWidth = '100px';
  public paramsChild = '';
  public fname = 'xxx';
  public lname = '';
  public ward_id = '';
  public ward_name = '';
  public user_login_name='';
  rawData = [];

  private list = new BehaviorSubject<string[]>([]);
  readonly list$ = this.list.asObservable();

  constructor() {}

  addNewList(list:never) {
    // console.log(list);
    this.rawData.push(list);
    this.list.next(this.rawData);
  }
  async userLoginName(){
    await this.user_login_name 
  }

  removeList(list:never) {
    this.rawData = this.rawData.filter(v => v !== list);
    this.list.next(this.rawData);
  }
}